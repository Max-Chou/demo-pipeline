#!/usr/bin/python3
import os

print("Gitlab Project Dir: {}".format(os.environ.get("CI_PROJECT_DIR")))
print("Username: {}".format(os.environ.get("USERNAME")))
print("DB Username: {}".format(os.environ.get("DB_USER")))
print("Mask Username: {}".format(os.environ.get("MASK_USER")))

